package com.discovery.darchrow.mybatis.mapper;

import java.util.List;

import com.discovery.darchrow.mybatis.model.User;

public interface UserMapper {
	
	public User selectUserByID(int id);
	
	public List<User> selectUsers(String userName);
}