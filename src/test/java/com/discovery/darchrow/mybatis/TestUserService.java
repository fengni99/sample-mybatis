/**
 * Copyright (c) 2016 Baozun All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Baozun.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Baozun.
 *
 * BAOZUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. BAOZUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */
package com.discovery.darchrow.mybatis;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.discovery.darchrow.mybatis.api.DemoUserService;
import com.discovery.darchrow.tools.jsonlib.JsonUtil;

public class TestUserService extends BaseTest{
	
	private Logger log =LoggerFactory.getLogger(TestUserService.class);
	
	@Autowired
	private DemoUserService userService;
	
	@Value("${jdbc.driverClassName}")
	private String jdbc;

	@Test
	public void test(){
		log.debug("sss:{}", JsonUtil.format(userService.findById(1)));
	}
	@Test
    public void test2(){
        log.debug("sss:{}", jdbc);
    }
	
}
