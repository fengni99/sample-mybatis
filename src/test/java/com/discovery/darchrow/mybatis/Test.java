/**
 * Copyright (c) 2016 Baozun All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Baozun.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Baozun.
 *
 * BAOZUN MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
 * SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE, OR NON-INFRINGEMENT. BAOZUN SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
 * THIS SOFTWARE OR ITS DERIVATIVES.
 *
 *//*
package com.discovery.darchrow.mybatis;

import java.io.Reader;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.discovery.darchrow.mybatis.model.IUserOperation;
import com.discovery.darchrow.mybatis.model.User;


public class Test {
    private static SqlSessionFactory sqlSessionFactory;
    private static Reader reader; 

    static{
        try{
            reader    = Resources.getResourceAsReader("Configuration.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static SqlSessionFactory getSession(){
        return sqlSessionFactory;
    }
    
    public static void main(String[] args) {
    	SqlSession session = sqlSessionFactory.openSession();
    	 一、session.selectOne 
        
        try {
        User user = (User) session.selectOne("com.discovery.darchrow.mybatis.models.UserMapper.selectUserByID", 1);
        print(user);
        } finally {
        session.close();
        }
    	
    	 二、面向接口编程
    	 SqlSession session = sqlSessionFactory.openSession();
         try {
             IUserOperation userOperation=session.getMapper(IUserOperation.class);
             User user = userOperation.selectUserByID(1);
             print(user);
         } finally {
             session.close();
         }
         
    	
    	 三、查list
         try {
             IUserOperation userOperation=session.getMapper(IUserOperation.class);          
             List<User> users = userOperation.selectUsers("%m%");
             print(users);
             
         } finally {
             session.close();
         }
         
    }
    
    public static void print(User user){
    	System.out.println(user.getUserName());
        System.out.println(user.getPassword());
    }
    
    public static void print(List<User> users){
    	users.forEach(System.out::println);
    }
}
*/